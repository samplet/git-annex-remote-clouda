;;; git-annex-remote-clouda
;;; Copyright 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of git-annex-remote-clouda.
;;;
;;; git-annex-remote-clouda is free software: you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; git-annex-remote-clouda is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with git-annex-remote-clouda.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (git-annex-remote-clouda remote)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-26)
  #:export (<remote>
            make-remote
            remote?
            config-ref
            config-set!
            run-remote))

(define-record-type <remote>
  (make-remote init prepare store retrieve present? remove)
  remote?
  (init remote-init)
  (prepare remote-prepare)
  (store remote-store)
  (retrieve remote-retrieve)
  (present? remote-present?)
  (remove remote-remove))

(define* (message-part message k #:key (last? #f))
  (let loop ((part-index 0) (message-index 0))
    (if (= part-index k)
        (let ((end (if last?
                       (string-length message)
                       (or (string-index message #\space message-index)
                           (string-length message)))))
          (substring message message-index end))
        (let ((space-index (string-index message #\space message-index)))
          (if space-index
              (loop (1+ part-index) (1+ space-index))
              #f)))))

(define (make-config)
  (lambda (message . args)
    (case message
      ((ref)
       (match-let (((key) args))
         (format #t "GETCONFIG ~a~%" key)
         (message-part (read-line) 1 #:last? #t)))
      ((set!)
       (match-let (((key value) args))
         (format #t "SETCONFIG ~a ~a~%" key value))))))

(define (config-ref config key)
  (config 'ref key))

(define (config-set! config key value)
  (config 'set! key value))

(define (run-remote remote)
  (define config (make-config))
  (setvbuf (current-output-port) 'line)
  (format #t "VERSION 1~%")
  (let loop ((line (read-line)) )
    (unless (or (eof-object? line)
                (= 0 (string-length line)))
      (match (message-part line 0)
        ("INITREMOTE"
         (catch #t
           (lambda ()
             ((remote-init remote) config)
             (format #t "INITREMOTE-SUCCESS~%"))
           (lambda (key . args)
             (format #t "INITREMOTE-FAILURE ~a~%" (cons key args)))))
        ("PREPARE"
         (catch #t
           (lambda ()
             ((remote-prepare remote) config)
             (format #t "PREPARE-SUCCESS~%"))
           (lambda (key . args) 
             (format #t "PREPARE-FAILURE ~a~%" (cons key args)))))
        ("TRANSFER"
         (match (message-part line 1)
           ("STORE"
            (let ((blob-key (message-part line 2))
                  (file (message-part line 3 #:last? #t))) 
              (catch #t
                (lambda ()
                  ((remote-store remote) config blob-key file)
                  (format #t "TRANSFER-SUCCESS STORE ~a~%" blob-key))
                (lambda (key . args)
                  (format #t "TRANSFER-FAILURE STORE ~a ~a~%"
                          blob-key (cons key args))))))
           ("RETRIEVE"
            (let ((blob-key (message-part line 2))
                  (file (message-part line 3 #:last? #t)))
              (catch #t
                (lambda ()
                  ((remote-retrieve remote) config blob-key file)
                  (format #t "TRANSFER-SUCCESS RETRIEVE ~a~%" blob-key))
                (lambda (key . args)
                  (format #t "TRANSFER-FAILURE RETRIEVE ~a ~a~%"
                          blob-key (cons key args)))))))) 
        ("CHECKPRESENT"
         (let ((blob-key (message-part line 1)))
           (catch #t
             (lambda ()
               (if ((remote-present? remote) config blob-key)
                   (format #t "CHECKPRESENT-SUCCESS ~a~%" blob-key)
                   (format #t "CHECKPRESENT-FAILURE ~a~%" blob-key)))
             (lambda (key . args)
               (format #t "CHECKPRESENT-UNKNOWN ~a ~a~%"
                       blob-key (cons key args))))))
        ("REMOVE"
         (let ((blob-key (message-part line 1)))
           (catch #t
             (lambda ()
               ((remote-remove remote) config blob-key)
               (format #t "REMOVE-SUCCESS ~a~%" blob-key))
             (lambda (key . args)
               (format #t "REMOVE-FAILURE ~a ~a~%"
                       blob-key (cons key args))))))
        (_ (format #t "UNSUPPORTED-REQUEST~%")))
      (loop (read-line)))))

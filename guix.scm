(use-modules (git)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             ((guix build utils) #:select (with-directory-excursion))
             (guix build-system gnu)
             (guix gexp)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (ice-9 popen)
             (ice-9 textual-ports)
             (srfi srfi-1))

(define *top-srcdir* (dirname (current-filename)))

(define *version*
  (with-directory-excursion *top-srcdir*
    (let* ((script "./build-aux/git-version-gen")
           (pipe (open-pipe* OPEN_READ script ".tarball-version"))
           (version (get-string-all pipe)))
      (close-pipe pipe)
      version)))

(define (make-select)
  (let* ((directory (repository-discover *top-srcdir*))
         (repository (repository-open directory))
         (oid (reference-target (repository-head repository)))
         (commit (commit-lookup repository oid))
         (tree (commit-tree commit))
         (paths (tree-list tree)))
    (lambda (file stat)
      (any (lambda (p) (string-suffix? p file)) paths))))

(package
  (name "git-annex-remote-clouda")
  (version *version*)
  (source (local-file *top-srcdir* #:recursive? #t #:select? (make-select)))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,guile-2.2)))
  (arguments
   '(#:phases
     (modify-phases %standard-phases
       (add-before 'configure 'bootstrap
         (lambda _
           (invoke "sh" "bootstrap")
           #t)))))
  (home-page #f)
  (synopsis "Use @uref{http://clouda.ca, Cloud-A} bulk storage as a
@code{git-annex} special remote")
  (description "This script provides an interface for @code{git-annex}
to use the bulk storage service from @uref{clouda.ca, Cloud-A} as a
special remote.")
  (license license:gpl3+))
